package academits.dubchak;

import academits.dubchak.coverter.ContactValidationConverter;
import academits.dubchak.coverter.ContactConverter;
import academits.dubchak.dao.ContactDao;
import academits.dubchak.service.ContactService;

/**
 * Created by Anna on 14.06.2017.
 */
public class PhoneBook {

    public static ContactDao contactDao = new ContactDao();

    public static ContactService phoneBookService = new ContactService();

    public static ContactConverter contactConverter = new ContactConverter();

    public static ContactValidationConverter contactValidationConverter = new ContactValidationConverter();
}
